﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PledgeSDDRequestCore.Const
{
    public class ConstApplication
    {

        public const int Const_Pro_Cat_Ade = 8;
        public const int Const_Pro_Cat_Don = 7;
        public const string Const_CultureLang = "it-IT";
        public const int Const_SqlTimeout = 30;
        public const int Const_SqlTimeoutExtended = 3600;
        public const int Const_MessageLength = 255;
        public const int Const_CurrencyDecimalDigits = 2;
        public const char Const_ParameterValueSeparator = '=';
        public const char Const_ParameterSeparator = '|';
        public const string Const_CurrencyDecimalSeparator = ",";
        public const string Const_BatchExecutor = "SYSTEM";
        public const string Const_TestMode = "N";
        public const string JobError = "Job Terminato con errore controllare nell'Event Viewer";
        public const string JobOk = "Job Terminato OK";
        public const string UpdateStateJob = "UpdateStateJob.sql";
        //    public const string LogOutcomeAAR = "RID_BANCA_1_ESI";
        public const string LogProgramError = "PROCEDURA INTERROTTA";
        public const string CheckDonationExistence = "RID_CheckDonationExistence.sql";
        public const string CheckLastOutcomeAccession = "RID_CheckLastOutcomeAccession.sql";
        public const string CheckException5003 = "RID_CheckException5003.sql";
        public const string CheckCodRID = "RID_CheckCodRID.sql";
        public const string CheckCodRIDOFF = "RID_CheckCodRIDONEOFF.sql";
        public const string GetNmbOrdFromNmbRidONEOFF = "RID_GetNmbOrdFromNmbRidONEOFF.sql";
        //  public const string UpdateStateAccession = "RID_UpdateStateAccession.sql";
        public const string UpdateStateAccessionChild = "RID_UpdateStateAccessionChild.sql";
        public const string UpdateStateAccessionNoChild = "RID_UpdateStateAccessionNoChild.sql";
        public const string LogHeader = "GESTIONE RICHIESTE SDD PLEDGE";
        public const int LogMsgWCEnd = 31;
        public const int LogMsgWCCreate = 10;
        public const int LogMsgWNCEnd = 32;
        public const int LogMsgWNCCreate = 7;
        public const string LogPledgeNextDonationStart = "Inizio Pledge Rate Successive";
        public const string LogPledgeFirstDonationStart = "Inizio Pledge Prima Rata";
        public const string Pledge_WithChild = "RID_PledgewithChild.sql";
        public const string Pledge_WithNoChild = "RID_PledgewithNoChild.sql";
        public const string SP_CreateDonationChild = "CREATE_DONATION_CHILD_RBK";
        public const string SP_CreateDonationNoChild = "CREATE_DONATION_NOCHILD_RBK";
        public const string SP_Create_Donor_Banking = "CREATE_DONOR_BANKING";
        public const string SP_Create_Outcome_90430 = "CREATE_OUTCOME_RBK_AAR";
        public const string SP_Create_Outcome_AAR = "CREATE_OUTCOME_RBK";
        public const string SP_Create_Outcome_INC = "CREATE_OUTCOME_RBK_INC_SEPA";
        public const string SP_Create_Outcome_INC_ONEOFF = "CREATE_OUTCOME_RBK_INC_SEPA_ONEOFF";
        public const string SP_Create_Request_AAR = "CREATE_REQUEST_RBK";
        public const string SP_Create_outcome_RBK_50003 = "CREATE_OUTCOME_RBK_50003_SEPA";
        public const string Check_BP_AccNmb = "RID_Check_BP_AccNmb.sql";
        public const string Check_Flux_Elaborated = "RID_Check_Flux_Elaborated.sql";
        public const string Request_AAR = "RID_Request_AAR.sql";
        public const string LogMsg46Cod_RID = "Codice RID";
        public const string LogMsg46Com_Name = "Cognome del committente";
        public const string LogMsg46Acc_Nmb = "Appoggio bancario del pagante";
        public const string LogMsg46Cod_Cin = "Codice CIN";
        public const string LogMsg46Holder = "Intestatario conto del pagante";
        public const string LogMsg46CF = "Codice Fiscale";
        public const string LogMsg46IBAN = "IBAN";
        public const string UpdateBPAccNmb = "RID_UpdateBPAccNmb.sql";
        public const string UpdateOrderNote = "RID_UpdateOrderNote.sql";
        public const string Request_INC_1th = "RID_Request_INC_1th.sql";
        public const string Request_INC_next = "RID_Request_INC_next.sql";
        public const string Request_INC_1th_SEPA = "RID_Request_INC_1th_SEPA.sql";
        public const string Request_INC_next_SEPA = "RID_Request_INC_next_SEPA.sql";
        public const string GetSchedulingAccession = "RID_GetSchedulingFromAccession.sql";
        public const string Req_inc_LOG = "L";
        public const string Req_inc_EXECUTE = "E";
        public const string SP_Create_Request_INC = "CREATE_REQUEST_RBK_INC_SEPA";
        public const string KEY_decoding = "UNICEF";
        public const string UpdateStateDateAccession = "RID_UpdateStateDateAccession.sql";
        public const string TerminateAccessionOutcomeSequence = "RID_TerminateAccessionOutcomeSequence.sql";
        public const string Finish_Accession_90420 = "RID_Finish_Accession_90420.sql";
        public const string AddFluxElaborated = "RID_Add_Flux_Elaborated.sql";
        public const string Set_NmbRid = "RID_Set_NmbRid.sql";
        public const string Msg_Rid_Code = "Il prefisso per il codice RID è maggiore della lunghezza massima (";
        public const int Rid_Code_Lenght = 5;
        public const string Update_Log = "RID_Update_Log.sql";
        public const string GetLast_BnkAcc = "RID_GetLastBnkAcc.sql";
        public const string Check_Accession_ReqAAR = "RID_Check_Accession_ReqAAR.sql";
        public const string SP_Manage_Donor_Banking = "MANAGE_DONOR_BANK_TYPE";
        public const string SP_Create_Outcome_91311 = "CREATE_OUTCOME_RBK_91311";
        public const string Request_Rev_AAR = "RID_Request_Rev_AAR.sql";
        public const string CheckSEPA = "RID_Check_SEPA.sql";
        public const string Check_outcome_positive = "RID_Check_outcome_positive.sql";
        public const string Get_Last_Outcome = "RID_Get_Last_Outcome.sql";
        public const string Get_flag_send_SEPA = "RID_Get_flag_send_SEPA.sql";
        public const string SP_CHECK_ACCESSION_NEGATIVE_CONSECUTIVE_RID = "CHECK_ACCESSION_NEGATIVE_CONSECUTIVE_RID";

        public const string Get_Bank_AAR = "RID_Get_Bank_AAR.sql";
        public const string Get_Last_IBAN = "RID_Last_IBAN.sql";
        public const string SP_CLOSE_DONATIONS_TERMINATED_ACCESSION = "CLOSE_DONATIONS_TERMINATED_ACCESSION";
        public const string GetDepositRequest = "RID_GetDepositRequest.sql";
        public const string CheckDepositOutcomeExsist = "RID_CheckDepositOutcomeExsist.sql";
        public const string GetDepositRequestONEOFF = "RID_GetDepositRequestONEOFF.sql";
        public const string CheckDepositOutcomeExsistONEOFF = "RID_CheckDepositOutcomeExsistONEOFF.sql";

        public const string Get_Donation_Nmb_Progressive = "RID_Get_Donation_Nmb_Progressive.sql";
        public const string Get_Donation_Nmb_Negative_Outcome = "RID_Get_Donation_Nmb_Negative_Outcome.sql";
        public const string Check_APPCO = "Rid_Check_APPCO.sql";
        public const string CheckAmendment = "CheckAmendment.sql";
        public const string CheckPaymentFromNmbRid = "Rid_Check_Payment_From_NmbRid.sql";
        public const string CheckPaymentFromDeposit = "Rid_Check_Payment_From_Deposit.sql";



        //  QUERY
        public const string PledgeSDD_CheckDonorBanking = "PledgeSDD_Check_DonorBanking.sql";
        public const string PledgeSDD_GetRequest = "PledgeSDD_GetRequest.sql";
        public const string PledgeSDD_InsertTempBankPercent = "PledgeSDD_InsertTempBankPercent.sql";
        public const string PledgeSDD_GetRequestElaboration = "PledgeSDD_GetRequestElaboration.sql";
        public const string PledgeSDD_ClearTempRequest = "PledgeSDD_ClearTempRequest.sql";
        public const string PledgeSDD_ClearTempPreElab = "PledgeSDD_ClearTempPreElab.sql";
        public const string PledgeSDD_CloseTempRequest = "PledgeSDD_CloseTempRequest.sql";
        public const string PledgeSDD_CheckMadateCode = "PledgeSDD_CheckMandateCode.sql";
        public const string PledgeSDD_GetBank = "PledgeSDD_GetBank.sql";
        public const string PledgeSDD_CheckMandateBlock = "PledgeSDD_CheckMandateBlock.sql";
        public const string PledgeSDD_GetFileElaboration = "PledgeSDD_GetFileElaboration.sql";
        public const string PledgeSDD_UpdateTempRequest = "PledgeSDD_UpdateTempRequest.sql";
        public const string PledgeSDD_InsertTempHeader = "PledgeSDD_InsertTempHeader.sql";
        public const string PledgeSDD_UpdateTempRollback = "PledgeSDD_UpdateTempRollback.sql";
        public const string PledgeSDD_GetBankAccount = "PledgeSDD_GetBankAccount.sql";
        public const string PledgeSDD_ClearTempExtractionRequest = "PledgeSDD_ClearTempExtractionRequest.sql";
        public const string PledgeSDD_RequestElaboration = "PledgeSDD_RequestElaboration.sql";
        public const string PledgeSDD_GetLogId = "PledgeSDD_GetLogId.sql";
        //STORE PROCEDURE
        public const string PledgeSDD_SP_Create_Request = "PLEDGE_SDD_CREATE_REQUEST";








        // Utility
        public const string LastDispCode = "EF";

        public const string char_text = "c";
        public const string char_int = "i";
        public const string char_date = "d";
        public const char space = ' ';
        public const char zero = '0';
        public const string node_sepatator = "/";



        // Query --- StoreProcedure
        public const string SP_Check_BNK_BRN = "CHECK_BNK_BRN";//iii
        public const string SP_GET_NEXT_WORKING_DAY = "GET_NEXT_WORKING_DAY";
        public const string SP_CHECK_IBAN = "CHECK_IBAN";
        public const string SP_CHECK_CF = "CHECK_CF";
        public const string SP_Update_Order_Core_RBK = "UPDATE_ORDER_CORE_RBK";
        public const string RID_Check_nmb_rid = "RID_Check_nmb_rid.sql";
        public const string RID_Check_Outcome_Type = "RID_Check_Outcome_Type.sql";
        public const string RID_Get_nmb_ord_From_nmb_rid = "RID_Get_nmb_ord_From_nmb_rid.sql";
        public const string RID_GetPayerHolder = "RID_GetPayerHolder.sql";
        public const string RID_GetBranchNmb = "RID_GetBranchNmb.sql";
        public const string RID_GetPaymentType = "RID_GetPaymentType.sql";
        public const string RID_Get_Donation_From_nmb_rid = "RID_Get_Donation_From_nmb_rid.sql";
        public const string RID_GetLastBnkAcc = "RID_GetLastBnkAcc.sql";


















    }
}
