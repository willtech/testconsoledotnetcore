﻿using PledgeSDDRequestCore.Class;
using Microsoft.Extensions;
using System;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions;
using Microsoft.Extensions.Configuration;

namespace PledgeSDDRequestCore
{
    class Program
    {
        private static IServiceCollection ConfigureServices(string[] args)
        {
            IServiceCollection services = new ServiceCollection();

            // Set up the objects we need to get to configuration settings
            var config = LoadConfiguration(args);

            services.Configure<IConfiguration>(config);
            services.Configure<GlobalSection>(config.GetSection("GlobalSection"));
            services.AddSingleton(config);

            services.AddLogging(logging =>
            {
                logging.AddConfiguration(config.GetSection("Logging"));
                logging.AddConsole();
            }).Configure<LoggerFilterOptions>(options => options.MinLevel =
                                              LogLevel.Debug);


            // services.AddTransient<ISchedulerService, SchedulerService>(); per passare una mia classe alla mia applicazione
            // IMPORTANT! Register our application entry point
            services.AddTransient<ManageRequest>();
            return services;
        }

        public static IConfiguration LoadConfiguration(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            //.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
            //.AddEnvironmentVariables()
            .AddCommandLine(args);
            return builder.Build();
        }


        [STAThread]
        static void Main(string[] args)
        {

            var services = ConfigureServices(args);
            // Generate a provider
            var serviceProvider = services.BuildServiceProvider();

            // Kick off our actual code
            serviceProvider.GetService<ManageRequest>().Run(args);

         

        }
        
    }
}
