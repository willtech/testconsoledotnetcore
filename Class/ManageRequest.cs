﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace PledgeSDDRequestCore.Class
{
    class ManageRequest
    {
        private readonly IOptions<GlobalSection> _globalSection;

       

        public ManageRequest(ILogger<ManageRequest> logger, IOptions<GlobalSection> globalsection)
        {
            this._globalSection = globalsection;
            logger.LogInformation("prova");
        }

        public async void Run(string[] args)
        {
            Console.WriteLine(_globalSection.Value.testpath);
            Console.ReadLine();

        }
    }
}
